import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { UsuarioDataI } from '../interfaces/usuario.interface';
import {  tap } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  listUsuarios: UsuarioDataI[] = [
    {usuario: 'Lucas Alaiaga', target: "2598755243345354", date: '01/20', key: "123"},
    {usuario: 'Juan Carlos', target: "4878925432147851", date: '05/21', key: "456"},
    {usuario: 'Tomas Ruiz Díaz', target: "5216589548458789", date: '11/22', key: "789"},
    {usuario: 'María Cortes', target: "1278252548954756", date: '06/25', key: "123"}
  ];




  constructor() { }



  getUsuario(): UsuarioDataI[]{
    //Slice retorna una copia del array
    return this.listUsuarios.slice();
  }

  eliminarUsuario(usuario: string){
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.usuario !== usuario;
    });
  }

  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);

  }

  modificarUsuario(user: UsuarioDataI){
    this.eliminarUsuario(user.usuario);
    this.agregarUsuario(user);
  }
}
